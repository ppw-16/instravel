from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.contrib.auth.models import User

# Create your tests here.
class DaftarDataTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response= Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_apakah_ada_button(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('button', content)

    def test_url_login(self):
        c = Client()
        response = c.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        c = Client()
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        data = {'username':'test', 'password':'test'}

        response = c.post('/login/', data=data)

        response2 = c.get('/login/')
        self.assertEqual(response2.status_code, 302)

        response3 = c.get('/')
        self.assertEqual(response3.status_code, 200)

    # def test_not_logged_in(self):
    #     c = Client()
    #     response = c.get('/')
    #     self.assertEqual(response.status_code, 302)

    def test_url_logout(self):
        c = Client()
        response = c.get('/logout/')
        self.assertEqual(response.status_code, 302)