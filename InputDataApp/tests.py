from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import inputdata
from .models import tempatWisata

# Create your tests here.
class DaftarDataTest(TestCase):
    def test_input_data_url_is_exist(self):
        response= Client().get('/input/')
        self.assertEqual(response.status_code, 200)
    
    def test_input_data_function(self):
        found = resolve('/input/')
        self.assertEqual(found.func, inputdata)

    def test_input_data_template(self):
        response = Client().get('/input/')
        self.assertTemplateUsed(response, 'InputData.html')

    def test_form_is_exist(self):
        response= Client().get('/input/')
        content= response.content.decode('utf8')
        self.assertIn('form', content)
        self.assertIn('<form', content)

    def test_apakah_ada_button_add(self):
        c = Client()
        response = c.get('/input/')
        content = response.content.decode('utf8')
        self.assertIn('<button', content)
        self.assertIn('Add', content)

    # def tempatWisata_test(self, nama="only a test", tempat="yes, this is only a test", harga="yes, this is only a test", musim="yes, this is only a test", deskripsi="yes, this is only a test"):
    #     return tempatWisata.objects.create( nama=nama, tempat=tempat, harga=harga, musim=musim, deskripsi=deskripsi)

    # def tempatwisata_creation(self):
    #     w = tempatWisata.objects.create( nama="only a test", tempat="yes, this is only a test", harga="<500000", musim="yes, this is only a test", deskripsi="yes, this is only a test")
    #     self.assertTrue(isinstance(w, tempatWisata))
    #     self.assertEqual(w.__unicode__(), w.nama)