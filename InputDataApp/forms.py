from django import forms
from .models import tempatWisata,tempatdimau
from django.forms import widgets

class ContactForm(forms.Form):
		nama=forms.CharField(
        label = 'Nama', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Nama Tempat Wisata",}),
    )
		tempat =forms.CharField(
        label = 'Lokasi', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Lokasi Tempat Wisata",}),
    )
		harga = forms.ChoiceField(
        label='Harga',
        choices=[('< Rp500.000', '< Rp 500.000'),
                ('Rp500.000-Rp1.000.000', 'Rp500.000-Rp1.000.000'),
                ('Rp1.000.000-Rp2.0000.000','Rp1.000.000-Rp2.0000.000'),
                ('Rp2.000.000-Rp3.0000.000','Rp2.000.000-Rp3.0000.000')],
        widget = forms.Select(attrs = {'class' : 'form-control', 'style': 'color: #09B291;'})
    )
		musim = forms.ChoiceField(
        label='musim',
        choices=[('Winter', 'Winter'),
                ('Summer', 'Summer'),],
        widget = forms.Select(attrs = {'class' : 'form-control', 'style': 'color: #09B291;'})
    )
		deskripsi = forms.CharField(
        label='Deskripsi Tempat Wisata',
        widget = forms.Textarea(attrs = {'class' : 'form-control','cols': 7, 'rows': 7, })
    )
		# foto= forms.ImageField(
		# 	widget =forms.FileInput(
		# 	attrs={'class': 'form-control-files'})
		# 	)
class meta:
		model = tempatWisata
		fields = ['nama','tempat','harga','musim','deskripsi']

class tempatform(forms.Form):
		nama=forms.CharField(
        label = 'Yuk, Tuliskan tempat wisata yang kamu ingin kunjungi', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Tulis disini ya",}),
    )
class meta2:
		model = tempatdimau
		fields = ['nama']