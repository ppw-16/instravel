from django.shortcuts import render,redirect
from django.http import Http404, HttpResponse
from .models import tempatWisata, tempatdimau
from .forms import ContactForm, tempatform
from django.http import JsonResponse
# Create your views here.
def inputdata(request):
	if request.user.is_authenticated:
		if request.method == "POST":
			form = ContactForm(request.POST)
			if form.is_valid():
				model_instance = tempatWisata(
					nama = form.data['nama'],
					tempat = form.data['tempat'],
					harga = form.data['harga'],
					musim = form.data['musim'],
					deskripsi = form.data['deskripsi'],
				# 	foto = form.data['foto']
				)
				model_instance.save()
				return redirect("/daftar/")
					
		else:
			form = ContactForm()
			

		argument={
			'form': form,
			'data': tempatdimau.objects.all().values(),
		}


		return render(request, "InputData.html",argument)	
	else:
		if request.method == "POST":
			form = tempatform(request.POST)
			if form.is_valid():
				model_instance = tempatdimau(
					nama = form.data['nama']
				# 	foto = form.data['foto']
				)
				model_instance.save()
					
		else:
			form = tempatform()
		argument={
			'form': form,
			'data': tempatdimau.objects.all(), 
		}


		return render(request, "InputData.html",argument)
