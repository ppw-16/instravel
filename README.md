[![pipeline status](https://gitlab.com/ppw-16/instravel/badges/master/pipeline.svg)](https://gitlab.com/ppw-16/instravel/commits/master)
[![coverage report](https://gitlab.com/ppw-16/instravel/badges/master/coverage.svg)](https://gitlab.com/ppw-16/instravel/commits/master)


PPW-E
Kelompok 16

Nama Anggota:
- Muhammad Alfi Syakir 	    (1806191364)
- Anisa Hasna Nabila 		(1806146865)
- Stephen Handiar Christian	(1806205703)
- Azkiya Hanna Rofifah 	    (1806191704)

Link HerokuApp:
https://instravel-web.herokuapp.com/

Aplikasi yang akan dibuat:
Aplikasi yang akan kami buat adalah aplikasi bertemakan pariwisata. Di dalam website tersebut berisikan tempat-tempat wisata yang ada di Indonesia. Daftar tempat wisata ini memiliki manfaat sebagai penampung atau wadah untuk menyimpan aset-aset berupa tempat wisata yang akan memudahkan masyarakat dan juga pelancong yang ingin berkunjung ke tempat tersebut untuk melihat kondisi serta informasi mengenai tempat wisata tersebut.

PEMBAGIAN TUGAS:


**1.  Muhammad Alfi Syakir: membuat [app daftar]**

Di dalam app “daftar” terdapat:
*  Fitur “Filter” daftar data tempat pariwisata berdasarkan Musim dan Harga
*  Fitur untuk menampilkan data-data tempat wisata yang telah diunggah
*  Memunculkan pop-up detail foto saat data di-klik
*  Fitur “Update Berita Pariwisata”
*  Menampilkan “Top 8 News Seputar Pariwisata di Indonesia” setiap harinya
*  Menerapkan Autentikasi, Sign in, Sign out

**2. Anisa Hasna Nabila: membuat [app qna]**

Di dalam app “qna” terdapat:
*  Menampilkan pertanyaan dan jawaban yang diajukan oleh pengguna
*  Fitur “form ask” untuk memasukkan pertanyaan yang akan pop-up ketika tombol “ask” diklik
*  Fitur “form answer” untuk menjawab pertanyaan yang ada ketika pengguna sudah log in
*  Menerapkan Autentikasi, Sign in, Sign out

**3. Stephen Handiar Christian : membuat [app inputDataApp]**

Di dalam app “InputDataApp” terdapat:
*  Menampilkan formulir mengenai data destinasi wisata berupa nama, lokasi, harga, musim, dan deskripsi ketika user sudah terautentikasi
*  Fitur “input data” untuk memasukan data yang akan ditampilkan di app daftar ketika mengklik button add,
*  Menampilkan formulir mengenai destinasi wisata yang ingin dilihat datanya oleh pengunjung , Ditampilkan saat pengguna belum terautentikasi
*  Fitur “ input tempat” ini untuk menampung saran destinasi yang belum ada di instravel supaya dapat di tampilkan dan daat meningkatkan layanan
*  Fitur “Ubah Tema” sebagai fitur eye-friendly untuk membantu user yang akan mengisi form dalam waktu lama sehingga mengurangi efek sakit pada mata akibat terlalu lama melihat layar
*  Menerapkan Autentikasi, Sign in, Sign out

**4. Azkiya Hanna Rofifah : membuat [app thingsToDo]**

Di dalam app “thingsToDo” terdapat :
*  Formulir untuk pengunjung dapat memberikan saran
*  Menampilkan saran yang telah diberikan oleh pengunjung-pengunjung ke dalam tabel
*  Menampilkan “things to do” yaitu hal-hal yang dapat dilakukan selama travelling yang ditampilkan dalam accordion
*  Menampilkan travel hacks yang hanya dapat diakses ketika pengguna telah melakukan sign in (pengguna telah terotentikasi). Travel hacks ditampilkan dalam accordion

