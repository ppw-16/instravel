
$(document).ready(function() {

    $('[id="daftar-box"]').hover(function() {
        $( this ).css("background-color", "52DE97");
        $( this ).css("color", "white");
	}, function() {
        $( this ).css("background-color", "white");
        $( this ).css("color", "black");
	});

    $('.dets').click(function(){
		$("#popup-detail").show();
	});

	$('[id="closedets"]').click(function(){
		$("#popup-detail").hide();
	});
});

$(document).ready(function(){
	$("#berita")[0].innerHTML = "<p class='m-auto mt-3 mb-3 text-align-center'>Klik Update Berita untuk menampilkan Top 8 News Hari ini<p>"

    $("#newsbtn").click(() => {
		search();

    })
});

function newsFormat(data){
    var {url, title} = data;
    return (
       " <a href="+url+" target='blank' class='no m-auto'><div class='item m-4'>"+
        "<div class='book-title text-align-center'><p class='p-1 m-0'>"+title+"</p>"+ "</div></div></a>"
    )
}

var itemPerPage=10;
var maxPage;
var currentPage;

function search(){
	$("#berita")[0].innerHTML = "<h4 class='m-auto mt-3 mb-3 text-align-center'>Daftar Berita:<h4>"
    $("#result")[0].innerHTML = "<p class='m-auto mt-3 mb-3 text-align-center'>Loading....<p>"
    $.ajax({
        method: 'GET',
        url: 'https://newsapi.org/v2/everything?q=tempat%wisata%indonesia&apiKey=afede7ec33db4cecaf22e185dc6aa898',
        success: function(result){
            maxPage = Math.floor(result.totalItems/itemPerPage);
			currentPage = 0;
            $("#result")[0].innerHTML = ""
            for(i=0; i<8; i++){
                $("#result")[0].innerHTML += newsFormat(result.articles[i]);
            }
        }
    })
}
