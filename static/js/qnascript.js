$(document).ready(function(){
	$("#askButton").click(function(){
		$("#myForm").show();
	});

	$("#closeButton").click(function(){
		$("#myForm").hide();
	});

	$('[id="kotakid"]').hover(function() {
		$( this ).css("opacity", "1");
	}, function() {
		$( this ).css("opacity", "0.5");
	});

	$('[id="kotak2id"]').hover(function() {
		$( this ).css("opacity", "1");
	}, function() {
		$( this ).css("opacity", "0.5");
	});

	ambildata();
});

function ambildata(){
	$.ajax({
		method: 'GET',
		url: '/qna/ambilData/',
		success: function(response) {
			console.log(response);
			for(let i=0; i<response.length; i++){
				var nama = response[i].nama;
				var email = response[i].email;
				var subjek = response[i].subjek;
				var pertanyaan = response[i].pertanyaan;

				$('#pertanyaan').append("<div class='kotak' id='kotakid'> <div class='row' style='font-size: 12px; padding: 0%'> <p>" + nama + "-" + email +"</p> </div>\
					<div class='row'><span style='background-color: #FFFFFF; color: #09B291'>" + subjek + "</span> </div> <div class='row'> <p>" + pertanyaan + "</p>\
					</div> <div class='row'> <hr class='line' style='border: 1px solid #FFFFFF;'> </div> </div> <br>");
			}
		}
	})
}