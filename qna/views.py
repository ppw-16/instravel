from django.shortcuts import render, redirect

from .forms import QnA_Form, Answer_Form
from .models import QnA_Model, Answer_Model

from rest_framework import viewsets
from .serializers import QnASerializer

from django.http import JsonResponse
import requests

# Create your views here.
class QnAViewSet(viewsets.ModelViewSet):
    queryset = QnA_Model.objects.all()
    serializer_class = QnASerializer

def index(request):
	qnaform = QnA_Form(request.POST or None)
	answerform = Answer_Form(request.POST or None)

	if request.method == 'POST':
		if qnaform.is_valid():
			qnaform.save()

			return redirect('qna:recent')

		elif answerform.is_valid():
			answerform.save()

			return redirect('qna:recent')


	qnamodel = QnA_Model.objects.all()
	answermodel = Answer_Model.objects.all()

	context = {
		'data_form' : qnaform,
		'data_form2': answerform,
		'data_model' : qnamodel,
		'data_model2': answermodel,
	}

	return render(request,'qna/qna.html', context)

def getData(request):
	# key = request.GET['key']
	url = 'https://instravel-web.herokuapp.com/qna/router/question/?format=json'

	response = requests.get(url)
	response_json = response.json()

	return JsonResponse(response_json, safe=False)

# def most(request):
# 	qnaform = QnA_Form(request.POST or None)

# 	if request.method == 'POST':
# 		if qnaform.is_valid():
# 			qnaform.save()

# 			return redirect('qna:recent')

# 	qnamodel = QnA_Model.objects.all()

# 	context = {
# 		'data_form' : qnaform,
# 		'data_model' : qnamodel,
# 	}
# 	return render(request,'qna/qna.html', context)

# def noanswer(request):
# 	qnaform = QnA_Form(request.POST or None)

# 	if request.method == 'POST':
# 		if qnaform.is_valid():
# 			qnaform.save()

# 			return redirect('qna:recent')

# 	qnamodel = QnA_Model.objects.all()

# 	context = {
# 		'data_form' : qnaform,
# 		'data_model' : qnamodel,
# 	}
# 	return render(request,'qna/qna.html', context)