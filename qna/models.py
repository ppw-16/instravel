from django.db import models

# Create your models here.
class QnA_Model(models.Model):
	nama = models.CharField(max_length=50)
	email = models.CharField(max_length=100)
	subjek = models.CharField(max_length=50)
	pertanyaan = models.CharField(max_length=500)

	def __str__(self):
		return "{}. {}".format(self.id, self.nama)

class Answer_Model(models.Model):
	penanya = models.CharField(max_length=50)
	jawaban = models.TextField(max_length=200)

	def __str__(self):
		return "{}. {}".format(self,id, self.penanya)