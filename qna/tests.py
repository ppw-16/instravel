from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .models import QnA_Model, Answer_Model
from .views import index, getData

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime
# Create your tests here.
class MyTest(TestCase):
	def test_apakah_ada_url_slash_qna(self):
		c = Client()
		response = c.get('/qna/')
		self.assertEqual(response.status_code, 200)

	def test_input_data_template(self):
		response = Client().get('/qna/')
		self.assertTemplateUsed(response, 'qna/qna.html')

	def test_apakah_pake_fungsi_index(self):
		found = resolve('/qna/')
		self.assertEqual(found.func, index)

	def test_apakah_punya_button_Ask(self):
		c = Client()
		response = c.get('/qna/')
		content = response.content.decode('utf8')
		self.assertIn("<button",content)
		self.assertIn("Ask",content)

	def test_apakah_punya_button_Answer(self):
		c = Client()
		response = c.get('/qna/')
		content = response.content.decode('utf8')
		self.assertIn("<button",content)
		self.assertIn("Answer",content)

	def test_apakah_ada_form(self):
		c = Client()
		response = c.get('/qna/')
		content = response.content.decode('utf8')
		self.assertIn("<form",content)
		self.assertIn("form",content)

	def test_QnA_Models(self, nama="budi", email="budi@gmail.com", subjek="tanya apa", pertanyaan="gajadi deh"):
		return QnA_Model.objects.create(nama=nama, email=email, subjek=subjek, pertanyaan=pertanyaan)

	def test_Answer_Models(self, penanya="budi", jawaban="bukan itu"):
		return Answer_Model.objects.create(penanya=penanya, jawaban=jawaban)

	def test_apakah_pake_fungsi_getData(self):
		found = resolve('/qna/ambilData/')
		self.assertEqual(found.func, getData)
# class functionalTest(LiveServerTestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')        
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(functionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(functionalTest, self).tearDown()

# 	def test_apakah_web_terbuka_dan_dapat_memasukkan_qna(self):
# 		selenium = self.selenium
# 		selenium.get("http://localhost:8000/qna/")
# 		tmp_asker = selenium.find_element_by_id('id_penanya')
# 		tmp_answer = selenium.find_element_by_id('id_jawaban')
# 		tmp_submit = selenium.find_element_by_id('id_button')

# 		tmp_asker.send_keys('Andi')
# 		tmp_answer.send_keys('Malioboro adalah salah satu kawasan anak muda Jogja')
# 		tmp_submit.send_keys(Keys.RETURN)
