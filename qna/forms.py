from django import forms
from .models import QnA_Model, Answer_Model

class QnA_Form(forms.ModelForm):
	class Meta:
		model = QnA_Model
		fields =[
			'nama',
			'email',
			'subjek',
			'pertanyaan',
		]

class Answer_Form(forms.ModelForm):
	class Meta:
		model = Answer_Model
		fields =[
			'penanya',
			'jawaban',
		]