from rest_framework import serializers

from .models import QnA_Model

class QnASerializer(serializers.ModelSerializer):
	class Meta:
		model = QnA_Model
		fields = '__all__'