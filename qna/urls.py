from django.contrib import admin
from django.urls import path, include
from . import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'question', views.QnAViewSet)

app_name = 'qna'

urlpatterns = [
    path('', views.index, name='qna'),
    path('recent/', views.index, name='recent'),
    path('router/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('ambilData/', views.getData),
]