from django import forms
from django.db import models
from .models import saranModel
class saranForm(forms.ModelForm):
    class Meta:
        model = saranModel
        fields = {'saran'}
        widgets = {
            'saran' : forms. TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            )
        }