from django.test import TestCase,Client, LiveServerTestCase
from django.urls import reverse,resolve
from .views import index
from .models import saranModel
from .forms import saranForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class SaranPage(TestCase):
    def test_apakah_ada_url_untuk_things_to_do(self):
        c = Client()
        response = c.get('/thingsToDo/')
        self.assertEqual(response.status_code,200)
    def test_apakah_file_html_index_digunakan(self):
        c = Client()
        response = c.get('/thingsToDo/')
        self.assertTemplateUsed(response, 'saran.html')
    def test_apakah_fungsi_index_dijalankan(self):
        response = resolve(reverse('thingsToDo'))
        self.assertEqual(response.func,index)
    def buat_model_saran_Model(self,nama="test nama", saran = "saran trial"):
        return saranModel.objects.create(nama=nama,saran=saran)
    def test_apakah_ada_model_saran_Model(self):
        k = self.buat_model_saran_Model()
        self.assertTrue(isinstance(k,saranModel))
        self.assertEqual(k.__unicode__(),k.nama)
    def test_apakah_fungsi_index_bekerja_saat_mendapat_request_GET(self):
        c = Client()
        response = c.get('/thingsToDo/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed('saran.html')
        self.assertIn("<form",content)
        self.assertIn('<input',content)
    def test_apakah_masukan_dengan_method_post_disimpan_dan_dimunculkan_kembali(self):
        c = Client()
        data = {'saran' : 'Coba method index post'}
        response = c.post('/thingsToDo/',data)
        content = response.content.decode('utf8')
        self.assertIn('Coba method index post',content)
        self.assertEqual(saranModel.objects.all().count(), 1)
        self.assertEqual(
            saranModel.objects.get(id=1).saran, 'Coba method index post'
        )
    def test_apakah_ada_accordian(self):
        c = Client()
        response = c.get('/thingsToDo/')
        content = response.content.decode('utf8')
        self.assertIn('accordian',content)
        self.assertIn('card-header',content)
        self.assertIn('card-body',content)
    

# Create your tests here.
