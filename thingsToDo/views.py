from django.shortcuts import render
from .forms import saranForm
from .models import saranModel

def index(request):
    dictio={}
    data = saranModel.objects.all()
    form = saranForm()

    if request.method == "POST":
        form = saranForm(request.POST or None)
        dictio['form']=form
        if form.is_valid():
            model = saranModel(saran=form['saran'].value())
            model.save()
            dictio['data'] = data
            return render(request,'saran.html',dictio)   
    elif data.count !=0 :
        return render(request, 'saran.html', {'form': form,'data':data})

# Create your views here.
