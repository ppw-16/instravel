from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormMusim, FormHarga
from .models import Filter
from InputDataApp.models import tempatWisata

# Create your views here.

def news(request):
    argument={
    }
    return render(request, 'news.html', argument)

def daftar(request):
    if request.method=="POST":
        
        fMusim= FormMusim(request.POST)
        fHarga= FormHarga(request.POST)
        if fMusim.is_valid() and fHarga.is_valid():
            data = tempatWisata.objects.filter(musim=fMusim.data['musim'], harga=fHarga.data['harga'])
            model_instance = Filter(
            harga = fHarga.data['harga'],
            musim = fMusim.data['musim'],
			)
            model_instance.save()
            
            argument= {
                'data_active': 'active',
                'musim': fMusim,
                'harga': fHarga,
                'data': data, 
            }               
        
    else:
        fMusim= FormMusim()
        fHarga= FormHarga()
        
        argument= {
            'data_active': 'active',
            'musim': fMusim,
            'harga': fHarga,
            'data': tempatWisata.objects.all().values(),
        }
    
    
    return render(request,'daftar.html', argument)

