from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import daftar
from .models import Filter

# from selenium import webdriver
# from django.test import LiveServerTestCase
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import time

# Create your tests here.
class DaftarDataTest(TestCase):
    def test_data_tempat_url_is_exist(self):
        response= Client().get('/daftar/')
        self.assertEqual(response.status_code, 200)
    
    def test_data_buku_function(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, daftar)

    def test_daftar_tempat_template(self):
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, 'daftar.html')

    def test_form_is_exist(self):
        response= Client().get('/daftar/')
        content= response.content.decode('utf8')
        self.assertIn('form', content)

    def test_apakah_ada_button_Filter(self):
        c = Client()
        response = c.get('/daftar/')
        content = response.content.decode('utf8')
        self.assertIn('<button', content)
        self.assertIn('Filter', content)

    def test_column_data_is_exist(self):
        response= Client().get('/daftar/')
        content= response.content.decode('utf8')
        self.assertIn('col', content)
    
    def test_models_daftar(self):
        Filter.objects.create(harga="test harga", musim="test musim")
        hitungjumlah = Filter.objects.all().count()
        self.assertEqual(hitungjumlah,1)

    def test_apakah_ada_popup(self):
        c = Client()
        response = c.get('/daftar/')
        content = response.content.decode('utf8')
        self.assertIn('popup-detail', content)
        self.assertIn('Close', content)

    def test_data_berita_url_is_exist(self):
        response= Client().get('/daftar/news/')
        self.assertEqual(response.status_code, 200)

    def test_daftar_berita_template(self):
        response = Client().get('/daftar/news/')
        self.assertTemplateUsed(response, 'news.html')

    def test_apakah_ada_button_kembali_dan_update(self):
        c = Client()
        response = c.get('/daftar/news/')
        content = response.content.decode('utf8')
        self.assertIn('<button', content)
        self.assertIn('Update Berita', content)
        self.assertIn('Kembali', content)

# class FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(chrome_options = chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

#     def test_uji_daftar_dan_news(self):
#         self.browser.get(self.live_server_url+ '/daftar/')

#         time.sleep(1)
#         to_news_btn = self.browser.find_element_by_id('newstombol')
#         to_news_btn.click()

#         time.sleep(1)
#         news_btn = self.browser.find_element_by_id('newsbtn')
#         news_btn.click()

        # time.sleep(1)
        # back_btn = self.browser.find_element_by_id('kembali')
        # back_btn.click()

        # time.sleep(1)



