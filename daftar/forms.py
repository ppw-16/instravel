from django import forms
from daftar import models

class FormMusim(forms.Form):
    musim = forms.ChoiceField(
        label='',
        choices=[('Winter', 'Winter'),
                ('Summer', 'Summer'),],
        widget = forms.Select(attrs = {'class' : 'form-control', 'style': 'border-color: orangered; color: orangered'})
    )
    class Meta:
        fields= ('musim')

class FormHarga(forms.Form):
    harga = forms.ChoiceField(
        label='Harga',
        choices=[('< Rp500.000', '< Rp 500.000'),
                ('Rp500.000-Rp1.000.000', 'Rp500.000-Rp1.000.000'),
                ('Rp1.000.000-Rp2.0000.000','Rp1.000.000-Rp2.0000.000'),
                ('Rp2.000.000-Rp3.0000.000','Rp2.000.000-Rp3.0000.000')],
        widget = forms.Select(attrs = {'class' : 'form-control',  'style': 'border-color: 52DE97; color: 52DE97'})
    )
    class Meta:
        fields= ('harga')
